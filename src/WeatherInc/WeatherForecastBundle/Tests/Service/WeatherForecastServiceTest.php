<?php

namespace WeatherInc\WeatherForecastBundle\Tests\Service;

use WeatherInc\WeatherForecastBundle\Service\WeatherForecastService;
use WeatherInc\WeatherForecastBundle\Storage\FileStorage;

class WeatherForecastServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var WeatherForecastService
     */
    private $weatherForecastService;

    public function setUp()
    {
        $storage = new FileStorage();
        $this->weatherForecastService = new WeatherForecastService($storage);
    }

    public function tearDown()
    {
        unset($this->weatherForecastService);
    }

    public function testGetForecasts()
    {
        //when
        $city = 'Gdansk';
        $days = array(
            "2014-09-23",
            "2014-09-24",
        );

        //given
        $forecasts = $this->weatherForecastService->getForecasts($city, $days);

        //then
        $this->assertCount(2, $forecasts[$city]);
        $this->assertArrayHasKey("2014-09-23", $forecasts[$city]);
    }
}
