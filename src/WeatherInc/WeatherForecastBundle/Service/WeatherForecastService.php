<?php

namespace WeatherInc\WeatherForecastBundle\Service;

use WeatherInc\WeatherForecastBundle\Entity\WeatherForecast;
use WeatherInc\WeatherForecastBundle\Storage\FileStorage;

class WeatherForecastService
{
    /**
     * @var FileStorage
     */
    private $storage;

    /**
     * @param FileStorage $storage
     */
    function __construct(FileStorage $storage)
    {
        $this->storage = $storage;
    }


    /**
     * @param string $city
     * @param array $days
     * @return WeatherForecast[]
     */
    public function getForecasts($city, array $days)
    {
        $forecasts = $this->storage->readAll($city, $days);

        return $forecasts;
    }
}
