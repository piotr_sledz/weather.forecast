<?php

namespace WeatherInc\WeatherForecastBundle\Storage;

use WeatherInc\WeatherForecastBundle\Entity\WeatherForecast;

class FileStorage
{
    /**
     * @var string
     */
    private $file;

    /**
     * @var array
     */
    private $data = array();

    function __construct()
    {
        $this->file = __DIR__ . '/../Resources/data/weatherForecasts.json';
        $this->loadData();
    }


    /**
     * @param string $city
     * @param array $days
     *
     * @return array
     */
    public function readAll($city, array $days)
    {
        $forecasts = array();
        foreach ($days as $day) {
            if (isset($this->data[$city][$day])) {
                $forecasts[$city][$day] = $this->data[$city][$day];
            }
        }

        return $forecasts;
    }

    private function loadData()
    {
        $stringData = file_get_contents($this->file);
        $rawData = json_decode($stringData, true);

        foreach ($rawData as $rowDataItem) {
            $weatherForecast = $this->mapRawDataToObject($rowDataItem);
            $this->data[$rowDataItem['city']]
                [$rowDataItem['day']]
                [$rowDataItem['timeOfDay']]  = $weatherForecast;
        }
    }

    /**
     * @param $rowDataItem
     *
     * @return WeatherForecast
     */
    private function mapRawDataToObject($rowDataItem)
    {
        $weatherForecast = new WeatherForecast();
        $weatherForecast->setCity($rowDataItem['city']);
        $weatherForecast->setCloudiness($rowDataItem['cloudiness']);
        $weatherForecast->setDay($rowDataItem['day']);
        $weatherForecast->setRainfall($rowDataItem['rainfall']);
        $weatherForecast->setTemperature($rowDataItem['temperature']);
        $weatherForecast->setTimeOfDay($rowDataItem['timeOfDay']);

        return $weatherForecast;
    }
}
