<?php

namespace WeatherInc\WeatherForecastBundle\Entity;

class WeatherForecast
{
    private $day;
    private $city;
    private $timeOfDay;
    private $temperature;
    private $cloudiness;
    private $rainfall;

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $cloudiness
     */
    public function setCloudiness($cloudiness)
    {
        $this->cloudiness = $cloudiness;
    }

    /**
     * @return mixed
     */
    public function getCloudiness()
    {
        return $this->cloudiness;
    }

    /**
     * @param mixed $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $rainfall
     */
    public function setRainfall($rainfall)
    {
        $this->rainfall = $rainfall;
    }

    /**
     * @return mixed
     */
    public function getRainfall()
    {
        return $this->rainfall;
    }

    /**
     * @param mixed $temperature
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;
    }

    /**
     * @return mixed
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * @param mixed $timeOfDay
     */
    public function setTimeOfDay($timeOfDay)
    {
        $this->timeOfDay = $timeOfDay;
    }

    /**
     * @return mixed
     */
    public function getTimeOfDay()
    {
        return $this->timeOfDay;
    }
}
