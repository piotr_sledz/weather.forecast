<?php

namespace WeatherInc\MainPageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use WeatherInc\WeatherForecastBundle\Service\WeatherForecastService;

class DefaultController extends Controller
{
    /**
     * @var string
     */
    private $dateTomorrow;

    /**
     * @var string
     */
    private $dateToday;

    function __construct()
    {
        $this->dateToday = date("Y-m-d");
        $this->dateTomorrow = date("Y-m-d", strtotime(date("Y-m-d") . "+1 day"));
    }


    /**
     * @param string $city
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($city)
    {
        $forecasts = $this->getForecasts($city);
        $twigParams = array(
            'forecastsToday' => $forecasts[$this->dateToday],
            'forecastsTomorrow' => $forecasts[$this->dateTomorrow],
            'city' => $city,
        );

        return $this->render('WeatherIncMainPageBundle:Default:index.html.twig', $twigParams);
    }

    /**
     * @param $city
     *
     * @throws NotFoundHttpException
     *
     * @return \WeatherInc\WeatherForecastBundle\Entity\WeatherForecast[]
     */
    private function getForecasts($city)
    {
        /** @var WeatherForecastService $weatherForecastService */
        $weatherForecastService = $this->get('weather_inc_weather_forecast.weather_forecast_service');

        $days = array(
            $this->dateToday,
            $this->dateTomorrow,
        );
        $forecasts = $weatherForecastService->getForecasts($city, $days);
        if (!isset($forecasts[$city])) {
            throw $this->createNotFoundException('Forecast for given city does not exist');
        }

        return $forecasts[$city];
    }
}
