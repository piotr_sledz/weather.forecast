<?php

namespace WeatherInc\MainPageBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/weather-forecast/Gdansk');

        $this->assertTrue($crawler->filter('html:contains("Gdansk")')->count() > 0);
    }
}
